<?php
//var_dump($_POST);
//die;

require_once("../../../vendor/autoload.php");

use App\ProfilePicture\ProfilePicture;

$objProfilePicture = new ProfilePicture();
if(!empty($_FILES['filepath']['name'])) {
    $imageName = time() . $_FILES['filepath']['name'];
    $tmp_location = $_FILES['filepath']['tmp_name'];
    $fileLocation = '../../../views/SEIP136344/ProfilePicture/img/'.$imageName;
    move_uploaded_file($tmp_location, $fileLocation);
    $_POST['filepath'] = $imageName;
}

$objProfilePicture->setData($_POST);
$objProfilePicture->update();
