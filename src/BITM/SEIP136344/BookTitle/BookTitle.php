<?php


namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{

    public $id="";

    public $book_title="";

    public $author_name="";


    public function __construct(){

        parent::__construct();

    }

//    public function index(){
//        echo "BookTitle found!";
//    }
    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){
            $this->id = $postVariabledata['id'];
        }

        if(array_key_exists('book_title',$postVariabledata)){
            $this->book_title = $postVariabledata['book_title'];
        }

        if(array_key_exists('author_name',$postVariabledata)){
            $this->author_name = $postVariabledata['author_name'];
        }
    }

    public function store(){

        $arrData = array($this->book_title, $this->author_name);

        $sql = "Insert INTO book_title(book_title,author) VALUES(?,?)";


        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully! :)");
        else
            Message::message("Failed! Data Has Not Been Inserted! :(");

        Utility::redirect('create.php');

    } //end of store method



    public function index($fetchMode='ASSOC'){
        $sql="SELECT * from book_title where is_deleted = 'No' ";

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){

        $arrData= array($this->book_title,$this->author_name);
        $sql="UPDATE book_title SET book_title = ?, author = ? WHERE id =".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    } //end of update()

    public function delete(){
        $sql= "DELETE FROM book_title WHERE id =".$this->id;
        $STH= $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    } // end of delete(), permanent delete

    public function trash(){

        $sql = "Update book_title SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from book_title where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed()





    public function recover(){

        $sql = "Update book_title SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover()

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();



    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_deleted` ='No' AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `is_deleted` ='No' AND `book_title` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_deleted` ='No' AND `author` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `book_title` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_title);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->author);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// end of get all keywords

    public function recoverSelected($IDs = Array())
    {

        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $sql = "UPDATE `book_title` SET `is_deleted` = 'No' WHERE `id` IN(" . $ids . ")";
            $STH = $this->DBH->prepare($sql);
            $result= $STH->execute();

            if ($result) {
                Message::message("
                  <div class=\"alert alert-success\">
                        <strong>Recovered!</strong> Selected Data has been recovered successfully.
                  </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            } else {
                Message::message("
                  <div class=\"alert alert-info\">
                       <strong>Failed!</strong> Selected Data has not been recovered successfully.
                 </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");
            Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function trashSelected($IDs = Array())
    {

        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $sql = "UPDATE `book_title` SET `is_deleted` = NOW() WHERE `id` IN(" . $ids . ")";

            $STH = $this->DBH->prepare($sql);
            $result= $STH->execute();

            if ($result) {
                Message::message("
                  <div class=\"alert alert-success\">
                        <strong>Trashed!</strong> Selected Data has been trashed successfully.
                  </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            } else {
                Message::message("
                  <div class=\"alert alert-danger\">
                       <strong>Failed!</strong> Selected Data has not been trashed successfully.
                 </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");
            Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }




    public function deleteMultiple($IDs = Array())
    {


        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $sql = "DELETE FROM `book_title` WHERE `id`  IN(" . $ids . ")";

            $STH = $this->DBH->prepare($sql);
            $result= $STH->execute();

            if ($result) {
                Message::message("
                 <div class=\"alert alert-info\">
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                 </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            } else {
                Message::message("
               <div class=\"alert alert-info\">
                   <strong>Deleted!</strong> Selected Data has not been deleted successfully.
               </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");

            Utility::redirect($_SERVER['HTTP_REFERER']);
        }

    }

}// end of BookTitle Class

?>

