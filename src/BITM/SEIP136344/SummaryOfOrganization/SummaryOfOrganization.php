<?php


namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class SummaryOfOrganization extends DB{

    public $id="";

    public $company_name="";

    public $summary="";



    public function __construct(){

        parent::__construct();

    }

//    public function index(){
//        echo "SummaryOfOrganization found!";
//    }
    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){
            $this->id = $postVariabledata['id'];
        }

        if(array_key_exists('company_name',$postVariabledata)){
            $this->company_name = $postVariabledata['company_name'];
        }

        if(array_key_exists('summary',$postVariabledata)){
            $this->summary = $postVariabledata['summary'];
        }
    }

    public function store(){

        $arrData = array($this->company_name, $this->summary);

        $sql = "Insert INTO summary_of_organization(company_name,summary) VALUES(?,?)";


        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully! :)");
        else
            Message::message("Failed! Data Has Not Been Inserted! :(");

        Utility::redirect('create.php');

    } //end of store method

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from summary_of_organization where is_deleted = 'No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from summary_of_organization where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of index();

    public function update(){

        $arrData= array($this->company_name,$this->summary);
        $sql="UPDATE summary_of_organization SET company_name = ?, summary = ? WHERE id =".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    } //end of update()

    public function delete(){
        $sql= "DELETE FROM summary_of_organization WHERE id =".$this->id;
        $STH= $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    } // end of delete(), permanent delete

    public function trash(){

        $sql = "Update summary_of_organization SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from summary_of_organization where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed()





    public function recover(){

        $sql = "Update summary_of_organization SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover()

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from summary_of_organization  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();



    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from summary_of_organization  WHERE is_deleted <> 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['bySummaryOfOrganizations']) )  $sql = "SELECT * FROM `summary_of_organization` WHERE `is_deleted` ='No' AND (`company_name` LIKE '%".$requestArray['search']."%' OR `summary` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['bySummaryOfOrganizations']) ) $sql = "SELECT * FROM `summary_of_organization` WHERE `is_deleted` ='No' AND `company_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['bySummaryOfOrganizations']) )  $sql = "SELECT * FROM `summary_of_organization` WHERE `is_deleted` ='No' AND `summary` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()



    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `summary_of_organization` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->company_name);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->company_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->summary);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->summary);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// end of get all keywords



}


